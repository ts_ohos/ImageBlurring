ImageBlurring
=============

本项目是基于开源项目qiujuer/ImageBlurring进行harmonyos化的移植和开发的，可以通过[github地址](https://github.com/qiujuer/ImageBlurring) 追踪到原项目.

## 项目介绍
HOS 中通过 Java 与 JNI 分别进行图片模糊；并且进行比较其运算速度。
#### 项目名称：ImageBlurring
#### 所属系列：harmonyos的第三方组件适配移植
#### 功能：通过 Java 与 JNI 分别进行图片模糊
#### 项目移植状态：未实现部分请参考[changelog](./CHANGELOG.md)
#### 调用差异：无
#### 原项目GitHub地址：https://github.com/qiujuer/ImageBlurring

## Thanks
[blurring](https://github.com/paveldudka/blurring)


## 相关文章

*  [csdn](http://blog.csdn.net/qiujuer/article/details/24282047)


## 三种方式

* `Fast Blur`
  > *  Java层进行模糊
  > *  `Fast Blur` 是国外一位开发者发明的模糊方法
  > *  `Fast Blur` 原名是 堆栈模糊

* `JniArray`
  > *  其模糊规则是 `堆栈模糊` 的 C 语言实现
  > *  在 JNI 层进行模糊，然后传回模糊后的数据
  > *  在 Java 层对图片类解析得到 像素点 数组传入到 JNI 层
  > *  JNI 层对像素点集合进行模糊，模糊后传回

* `JniBitMap`
  > *  其模糊规则是 `堆栈模糊` 的 C 语言实现
  > *  在 JNI 层进行模糊，然后传回模糊后的数据
  > *  在 JNI 层直接对图片模糊，模糊后传回


## 安装教程

Repository Add this in your root build.gradle file (not your module build.gradle file):
```java
allprojects {
	repositories {
		...
		mavenCentral()
	}
}
```

Dependency Add this to your module's build.gradle file:
```java
dependencies {
    ...
    implementation 'com.gitee.ts_ohos:image_blurring:1.0.0'
}
```

## 使用说明
使用java进行模糊处理
```java
    /**
     * pixelMap: 需要处理图片的PixelMap
     * radius: 模糊半径
     * boolean: 是否重用原始图片
     */
    StackBlur.blur(bitmap, radius, true);
```

使用JniArray进行模糊处理
```java
    /**
     * pixelMap: 需要处理图片的PixelMap
     * radius: 模糊半径
     * boolean: 是否重用原始图片
     */
    StackBlur.blurNativelyPixels(pixelMap, radius, true);
```

使用JniBitMap进行模糊处理
```java
    /**
     * pixelMap: 需要处理图片的PixelMap
     * radius: 模糊半径
     * boolean: 是否重用原始图片
     */
    StackBlur.blurNatively(pixelMap, radius, true);
```

## 效果展示

#### BLUR
| ![blur1](images/blur1.png) |![blur1](images/blur2.png)|
| :------------------------: | :------------------------: |

#### Animation
![Animation](images/anim.gif)

## 移植版本
Branches/master(2e4c3a2 on 21 Feb 2017)

## 版本迭代
- v1.0.0 harmonyos首次移植版本

License
--------

    Copyright 2014-2016 Qiujuer.
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

