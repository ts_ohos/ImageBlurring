# Changelog

## v.1.0.0
移植后首次上传

## support
1. Java层进行模糊
2. 在 Java 层对图片类解析得到 像素点 数组传入到 JNI 层进行模糊处理
3. 在 JNI 层进行模糊，然后传回模糊后的数据

## not support
1. 原库RenderScript在HOS无相应实现