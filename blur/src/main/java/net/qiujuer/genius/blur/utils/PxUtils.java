/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.qiujuer.genius.blur.utils;

import ohos.agp.components.Component;
import ohos.agp.render.Paint;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * Describe：尺寸工具类
 * Created by 吴天强 on 2017/9/19.
 */
public class PxUtils {
    /**
     * 得到设备屏幕的宽度
     *
     * @param context ctx
     * @return int
     */
    public static int getScreenWidth(Context context) {
        DisplayAttributes attributes = DisplayManager
                .getInstance()
                .getDefaultDisplay(context)
                .get()
                .getRealAttributes();
        return attributes.width;
    }

    /**
     * 得到设备屏幕的高度
     *
     * @param context ctx
     * @return int
     */
    public static int getScreenHeight(Context context) {
        DisplayAttributes attributes = DisplayManager
                .getInstance()
                .getDefaultDisplay(context)
                .get()
                .getRealAttributes();
        return attributes.height;
    }

    /**
     * 得到设备的密度
     *
     * @param context ctx
     * @return float
     */
    public static float getScreenDensity(Context context) {
        DisplayAttributes attributes = DisplayManager
                .getInstance()
                .getDefaultDisplay(context)
                .get()
                .getRealAttributes();
        return attributes.densityPixels;
    }

    /**
     * 把密度转换为像素
     *
     * @param vpValue dp值
     * @return int
     */
    public static int vp2px(Context context, float vpValue) {

        float scale = getScreenDensity(context);
        return (int) (vpValue * scale + 0.5);
    }

    /**
     * 将像素转换成vp
     *
     * @param pxValue px值
     * @return int
     */
    public static int px2vp(Context context, float pxValue) {

        float scale = getScreenDensity(context);
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * 将fp值转换为px值，保证文字大小不变
     *
     * @param fpValue sp值
     * @return int
     */
    public static int fp2px(Context context, float fpValue) {

        float fontScale = getScreenDensity(context);
        return (int) (fpValue * fontScale + 0.5f);
    }

    /**
     * 测量 View
     *
     * @param measureSpec
     * @param defaultSize View 的默认大小
     * @return
     */
    public static int measure(int measureSpec, int defaultSize) {
        int result = defaultSize;
        int specMode = Component.EstimateSpec.getMode(measureSpec);
        int specSize = Component.EstimateSpec.getSize(measureSpec);

        if (specMode == Component.EstimateSpec.PRECISE) {
            result = specSize;
        } else if (specMode == Component.EstimateSpec.NOT_EXCEED) {
            result = Math.min(result, specSize);
        }
        return result;
    }
    /**
     * 测量文字高度
     *
     * @param paint
     * @return
     */
    public static float measureTextHeight(Paint paint) {
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        return (Math.abs(fontMetrics.ascent) - fontMetrics.descent);
    }
}
