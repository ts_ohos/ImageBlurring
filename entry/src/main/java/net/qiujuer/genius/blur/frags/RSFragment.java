package net.qiujuer.genius.blur.frags;

import net.qiujuer.genius.blur.StackBlur;
import ohos.agp.components.AttrSet;
import ohos.app.Context;
import ohos.media.image.PixelMap;

public class RSFragment extends BaseFragment {

    public RSFragment(Context context) {
        super(context);
    }

    public RSFragment(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public RSFragment(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void show(long time) {
        super.show(time);
        mText.setText("RenderScript");
    }

    @Override
    protected PixelMap blur(PixelMap bitmap, int radius) {
        return StackBlur.blur(bitmap, radius, true);
    }
}
