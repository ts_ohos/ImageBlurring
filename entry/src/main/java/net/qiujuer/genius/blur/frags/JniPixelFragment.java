package net.qiujuer.genius.blur.frags;

import net.qiujuer.genius.blur.StackBlur;
import ohos.agp.components.AttrSet;
import ohos.app.Context;
import ohos.media.image.PixelMap;

public class JniPixelFragment extends BaseFragment {

    public JniPixelFragment(Context context) {
        super(context);
    }

    public JniPixelFragment(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public JniPixelFragment(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void show(long time) {
        super.show(time);
        mText.setText("Jni Pixel");
    }

    @Override
    protected PixelMap blur(PixelMap bitmap, int radius) {
        return StackBlur.blurNativelyPixels(bitmap, radius, true);
    }
}
