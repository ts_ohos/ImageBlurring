package net.qiujuer.genius.blur.frags;

import net.qiujuer.genius.blur.StackBlur;
import ohos.agp.components.AttrSet;
import ohos.app.Context;
import ohos.media.image.PixelMap;

public class JniBitmapFragment extends BaseFragment {

    public JniBitmapFragment(Context context) {
        super(context);
    }

    public JniBitmapFragment(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public JniBitmapFragment(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    @Override
    protected void show(long time) {
        super.show(time);
        mText.setText("Jni Bitmap");
    }

    @Override
    protected PixelMap blur(PixelMap bitmap, int radius) {
        return StackBlur.blurNatively(bitmap, radius, true);
    }
}
