package net.qiujuer.genius.blur.slice;

import net.qiujuer.genius.blur.ResourceTable;
import net.qiujuer.genius.blur.frags.BaseFragment;
import net.qiujuer.genius.blur.utils.PixelMapUtils;
import net.qiujuer.genius.blur.utils.PxUtils;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.RectFloat;
import ohos.agp.window.service.WindowManager;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;

public class MainAbilitySlice extends AbilitySlice
        implements Component.ClickedListener {

    BaseFragment mJniPixel;
    BaseFragment mJniBitmap;
    BaseFragment mJava;
    BaseFragment mRS;
    BaseFragment mAnim;
    DirectionalLayout mBgLayout;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_FULL_SCREEN);

        findFragments();
        findComponentById(ResourceTable.Id_btn_opt)
                .setClickedListener(this);
    }

    private void findFragments() {
        mJniPixel = (BaseFragment) findComponentById(ResourceTable.Id_frag_jni_pixel);
        mJniBitmap = (BaseFragment) findComponentById(ResourceTable.Id_frag_jni_bitmap);
        mJava = (BaseFragment) findComponentById(ResourceTable.Id_frag_java);
        mRS = (BaseFragment) findComponentById(ResourceTable.Id_frag_rs);
        mAnim = (BaseFragment) findComponentById(ResourceTable.Id_frag_anim);
        mBgLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_bg_layout);
    }

    private void start() {
        PixelMap bmp = PixelMapUtils
                .getPixelMapFromResource(getContext(),
                        ResourceTable.Media_picture);
        PixelMap bitmap = PixelMapUtils.createBitmap(mBgLayout.getWidth(),
                mBgLayout.getHeight(),
                PixelFormat.ARGB_8888);
        Canvas canvas = new Canvas(new Texture(bitmap));
        canvas.scale(1, 1);
        Paint paint = new Paint();
        paint.setFilterBitmap(true);
        canvas.drawPixelMapHolderRect(new PixelMapHolder(bmp), new RectFloat(0, 0,
                mBgLayout.getWidth(), mBgLayout.getHeight()), paint);
        setFragmentsBitmap(bitmap);
    }

    private void setFragmentsBitmap(final PixelMap bmp) {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                mJniPixel.setBitmap(bmp);
                mJniBitmap.setBitmap(bmp);
                mJava.setBitmap(bmp);
                mAnim.setBitmap(bmp);
//                mRS.setBitmap(bmp);
            }
        });
        thread.start();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        start();
        component.setVisibility(Component.HIDE);
    }
}
